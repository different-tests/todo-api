https://medium.com/@hienviluong125/part-1-building-and-deploying-a-rest-api-with-golang-clean-architecture-postgres-and-render-com-872a6140945f


docker run --name my-postgres -e POSTGRES_PASSWORD=password -p 5432:5432 -d postgres


docker exec -it my-postgres psql -U postgres -W


create database todo_db_development;
\c todo_db_development
CREATE TABLE todos (
    id SERIAL PRIMARY KEY,
    body VARCHAR(255) NOT NULL,
    completed BOOLEAN NOT NULL DEFAULT false
);
INSERT INTO todos (body) VALUES ('Learn golang');

go mod init gitlab.com/different-tests/todo-api

go get github.com/lib/pq
go get github.com/jmoiron/sqlx
go get github.com/gin-gonic/gin
go get github.com/kelseyhightower/envconfig
go get github.com/google/wire

echo 'DSN="postgres://postgres:password@127.0.0.1/todo_db_development?sslmode=disable"' >> local.env
echo 'GIN_PORT=":7070"' >> local.env

export $(cat local.env | xargs)

go run cmd/api/main.go


curl 'localhost:7070/todos/create' -H 'Content-Type: application/json' -d '{"body": "Create a new todo"}'

curl 'localhost:7070/todos/list' -H 'Content-Type: application/json'


curl https://todo-api-2phe.onrender.com/todos/ -H 'Content-Type: application/json' -d '{"body": "Create a new todo"}'