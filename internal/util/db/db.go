// internal/util/db/db.go
package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/different-tests/todo-api/internal/util/config"
)

func NewPostgresDB(conf *config.Config) (*sqlx.DB, error) {
 db, err := sqlx.Connect("postgres", conf.Dsn)
 if err != nil {
  return nil, err
 }

 return db, nil
}