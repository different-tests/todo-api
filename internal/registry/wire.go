// internal/registry/wire.go
// +build wireinject

package registry

import (
 "github.com/gin-gonic/gin"
 "github.com/google/wire"
 "gitlab.com/different-tests/todo-api/internal/handler"
 "gitlab.com/different-tests/todo-api/internal/repository"
 "gitlab.com/different-tests/todo-api/internal/service"
 "gitlab.com/different-tests/todo-api/internal/util/config"
 "gitlab.com/different-tests/todo-api/internal/util/db"
)

func NewGinServer(conf *config.Config) (*gin.Engine, error) {
 wire.Build(
  handler.SetupHandlers,
  handler.NewTodoHandler,
  service.NewTodoService,
  repository.NewTodoRepository,
  db.NewPostgresDB,
 )

 return nil, nil
}